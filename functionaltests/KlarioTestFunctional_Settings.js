/**
* Created by ravishankar on 8/11/16.
*/
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.url)

    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(2000)

    //click on Settings after verifying if it exists
    .exists('#settings')
    .click('a[href="#settings"]')
    .wait(2000)

    //verify if the monday toggle button exists and click
    .exists('klarioIN')
    .exists('klarioNET')
    .wait(2000)
    .click('[id="klarioNET"]')
    .wait(2000)

    //click About page and come back to settings page and verify that "Klario.Net is clicked
    .click('li[class="menuOptions"] a[href="#about"]')
    .wait(2000)
    .click('a[href="#settings"]')
    .wait(2000)

    // Verify notifications exist and toggle it
    .exists('notificationSettings')
    .wait(2000)
    .uncheck('[id="notificationSettings"]')
    .wait(2000)

    //click About page and come back to settings page and verify that "Turn notifications on" exist and toggle it again
    .click('li[class="menuOptions"] a[href="#about"]')
    .wait(2000)

    .click('a[href="#settings"]')
    .exists('notificationSettings')

    .wait(2000)
    .check('[id="notificationSettings"]')

    //click KlarioIn
    .click('[id="klarioIN"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });