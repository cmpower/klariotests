var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)

    //Login as user1
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //On the device
    .click('span[class="toggle"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

            })
    .end()
    .then(function (result) {
        console.log(result)
    });