/**
 * Created by ravishankar on 17/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)

    //Login as user1
    .type('form[id="loginForm"] [name=userId]', 'ppcloud41@gmail.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ppcloud41$')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Now click on the add device button
    .click('button[id="showAddDeviceFormButton"]')
    .wait(2000)

    //Type in serial ID and add device
    .type('input[placeholder="Serial No"]', '300004')
    .wait(2000)
    .click('button[type="submit"]')
    .wait(2000)

    //$$$$$$$$$$$$$$$$$$$$$$$$

    //Login as user2
    .goto(urlin.url)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'democloudmpower@gmail.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'demo123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Now click on the add device button
    .click('button[id="showAddDeviceFormButton"]')
    .wait(2000)
    //Type in serial ID and add device
    .type('input[placeholder="Serial No"]', '300004')
    .wait(2000)
    .click('button[type="submit"]')
    .wait(2000)

    //$$$$$$$$$$$$$$$$$$$$$$$$

    //login as the user1 and approve
    .goto(urlin.url)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'ppcloud41@gmail.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ppcloud41$')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)
    .exists('samp')

    //search for the device
    .type('input[type="search"]', 'app4')
    .wait(2000)

    //click on the permissions for that particular device
    .click('a[href="#user-approval"]')
    .wait(2000)
    //approve request
    .click('button[class="acceptUser btn btn-raised btn-sm btn-success btn-block"]')
    .wait(2000)

    //click on logoutButton after verifying if it exists
    .exists('logoutButton')
    .click('li[id="logoutButton"]')
    .wait(5000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
