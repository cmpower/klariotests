/**
 * Created by ravishankar on 15/11/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.url)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)

    //check remember me box and login
    .check('input[id="rememberUser"]')
    .wait(2000)
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(2000)

    //click on logoutButton after verifying if it exists
    .click('li[id="logoutButton"]')
    .wait(3000)

    //uncheck remember me
    .uncheck('input[id="rememberUser"]')
    .wait(2000)

    //Login again
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(5000)

    //verify if logout exists and logout
    .exists('logoutButton')
    .click('li[id="logoutButton"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });

