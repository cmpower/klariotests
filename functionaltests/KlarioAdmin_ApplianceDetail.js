/**
 * Created by ravishankar on 8/12/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //search for device
    .type('input[type="search"]', 'app3')
    .wait(2000)
    .exists('new1')
    //Click on the sorting icon
    .click('td[class="sorting_1"]')
    .wait(2000)

    //Click on appliance Details
    .click('[class="applianceDetailsBtn btn btn-sm btn-raised btn-info"]')
    .wait(2000)

    //verify if the appliance id exists
    .exists('h5[id="macId"]')
    .wait(2000)

    //click on close
    .click('[class="close"]')
    .wait(2000)

    //verify if revoke button exists
    .exists('[class="revokeSubOwner btn btn-sm btn-raised btn-danger"]')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });

