/**
 * Created by ravishankar on 30/11/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //Click on appliance menu
    .click('[class="dropdown-toggle f-18"]')
    .wait(2000)

    //Choose Log from appliance menu
    .click('a[href="#appliance-log"]')
    .wait(2000)

    //Search for a specific log
    .type('input[type="search"]', 'Appliance')
    .wait(2000)
    .exists('[class="sorting_1"]')
    .wait(2000)

    //click ont he details
    .click('[class="applianceLogDetailsBtn btn btn-sm btn-raised btn-info"]')
    .wait(2000)
    //verify if details exist
    .exists('h5[id="deviceAnomalyPacket"]')
    .exists('h5[id="deviceMetricsPacket"]')
    .wait(2000)

    //Close the window
    .click('[class="close"]')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });

