var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
	.goto(urlin.url)
	.wait(2000)
	//Login as demo
	.type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
	.type('form[id="loginForm"] [name=inputPassword]', 'ph123')
	.click('form[id="loginForm"]	[type="submit"]')
	.wait(2000)

	//click on add device after verifying if it exists
	.exists('showAddDeviceFormButton')

	//Now click on the add device button
	.click('button[id="showAddDeviceFormButton"]')
	.wait(2000)

	//click on logoutButton after verifying if it exists
	.click('li[id="logoutButton"]')
	.wait(2000)

	.evaluate(
		function () {
			return document.documentElement.innerText
			})
	.end()
	.then(function (result) {
		console.log(result)
	});


