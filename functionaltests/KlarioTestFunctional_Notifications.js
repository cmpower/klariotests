/**
 * Created by ravishankar on 16/11/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.url)
    .wait(2000)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)

    //check remember me box and login
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(2000)

    //click on Settings
    .click('a[href="#settings"]')
    .wait(2000)

    //Enable notifications
    .click('[id="notificationSettings"]')
    .wait(2000)

    //Go to My appliances
    .click('li[class="menuOptions"] a[href="#about"]')
    .wait(2000)
    .click('a[href="#appliance"]')
    .wait(2000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Enable a device and take a screenshot to verify if the notification has come
    .click('span[class="toggle"]')
    .screenshot('device-enable-notification.png')
    .wait(3000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
