/**
 * Created by ravishankar on 8/12/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //Click on appliance menu
    .click('[class="dropdown-toggle f-18"]')
    .wait(2000)

    //Choose Log from appliance menu
    .click('[href="#appliance-ssh"]')
    .wait(2000)

    //Search for a specific appliance
    .type('input[type="search"]', 'Appliance')
    .wait(2000)

    //click on startssh
    .click('[class="applianceStartSSH btn btn-sm btn-primary btn-raised"]')
    .wait(2000)

    //Search for a specific appliance
    .type('input[type="search"]', 'Appliance')
    .wait(2000)

    //click on stopssh
    .click('td[class="sorting_1"]')
    .click('[class="applianceStopSSH btn btn-sm btn-raised btn-info btn-danger"]')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });

