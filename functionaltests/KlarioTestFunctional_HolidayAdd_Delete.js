/**
 * Created by ravishankar on 14/11/16.
 */

var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.url)
    .wait(2000)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(2000)
    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Click calendar
    .click('i[class="fa fa-calendar fa-2x"]')
    .wait(2000)

    //click on holidaytab
    .click('a[href="#HolidaysTab"]')
    .wait(2000)

    //Add holiday name
    .click('button[id="showAddHolidayFormButton"]')
    .wait(2000)

    //Enter the holiday name
    .type('[id="holidayDesc"]', 'Specialday')
    .wait(2000)

    //select date
    .click('td[class="day"] span[class="calDate"]')
    .wait(2000)
    //save
    .click('button[class="btn bg-primarycolor c-white btn-raised"]')
    .wait(2000)

    //Go to Calendar tab and then come back to Holidays
    .click('a[href="#DateRangeTab"]')
    .wait(2000)
    .click('a[href="#HolidaysTab"]')
    .wait(2000)

    //verify if added holiday exists
    .exists('Specialday')
    .wait(2000)

    //delete the added holiday
    .click('[data-pos="7"]')
    .wait(2000)
    //Click OK
    .click('button[tabindex="0"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });
