/**
 * Created by ravishankar on 24/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login as user
    .type('form[id="loginForm"] [name=userId]', 'ppcloud41@gmail.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ppcloud41$')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'app4')
    .wait(2000)

    //Click device settings button
    .click('span[class="c-teal"]')
    .wait(6000)

    //DELETE the device
    .click('button[id="removerApplianceFromMyList"]')
    .wait(3000)

    //Click OK
    .click('button[tabindex="0"]')
    .wait(2000)

    //Go to appliance list
    .click('a[href="#appliance"]')
    .wait(2000)

    //Search for samp1
    .type('input[type="search"]', 'app4')
    .screenshot('Appliance-list-after-delete.png')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });