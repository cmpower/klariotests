var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Click calendar
    .click('i[class="fa fa-calendar fa-2x"]')
    .wait(2000)

    //click on LEGEND
    .click('span[class="lead"]')
    .wait(2000)

    //verify if the legend options exist
    .visible('From weekly schedule')
    .visible('Custom daily schedule')
    .visible('Today')
    .visible('Holiday')

    .wait(3000)
    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)

    });
