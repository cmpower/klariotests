/**
 * Created by ravishankar on 15/12/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //Click on appliance menu
    .click('[class="dropdown-toggle f-18"]')
    .wait(2000)

    //Choose Transaction from appliance menu
    .click('[href="#appliance-transaction-list"]')
    .wait(2000)

    //Search for a specific appliance
    .type('input[type="search"]', 'Appliance')
    .wait(2000)

    //click on show transaction button
    .click('[class="showApplianceTransaction btn btn-sm btn-raised btn-info"]')
    .wait(2000)

    //verify if the sorting class exists
    .exists('td[class="sorting_1"]')
    .wait(2000)

    //click on From time
    .click('[for="dateFromPicker"]')
    .wait(2000)
    //click on date
    .type('input[id="dateFromPicker"]', '12/01/2016')
    .wait(2000)

    //click on To time
    .click('[for="dateToPicker"]')
    .wait(2000)
    //click on date
    .type('input[id="dateToPicker"]', '12/15/2016')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });