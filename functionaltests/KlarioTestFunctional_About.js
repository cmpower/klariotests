var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(2000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(2000)

    //click on about after verifying if it exists
    .exists('About')
    .wait(2000)
    .click('li[class="menuOptions"] a[href="#about"]')
    .wait(2000)
    //Verify if header exists and also the text inside about page
    .exists('p[class="p-t-40 text-justify"]')
    .exists('Klario Management App')
    .visible('To enhance the performance of the app')
    .exists('© Klario')

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });
