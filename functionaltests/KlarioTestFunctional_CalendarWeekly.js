var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Click calendar
    .click('i[class="fa fa-calendar fa-2x"]')
    .wait(2000)

    //click on weeklytab
    .click('a[href="#WeeklyTab"]')
    .wait(2000)

    //verify if the monday toggle button exists and click
    .exists('monWeeklyButton')
    .wait(2000)
    .uncheck('input[id="monWeeklyButton"]')
    .wait(2000)
    //toggle tuesday
    .uncheck('input[id="tueWeeklyButton"]')
    .wait(2000)

    //toggle saturday
    .uncheck('input[id="satWeeklyButton"]')
    .wait(2000)

    //verify if the changes are saved
    .click('a[href="#DateRangeTab"]')
    .wait(2000)

    //Go back to weekly tab
    .click('a[href="#WeeklyTab"]')
    .wait(2000)

    //enable all the disabled buttons
    .check('input[id="monWeeklyButton"]')
    .wait(2000)
    .check('input[id="tueWeeklyButton"]')
    .wait(2000)
    .check('input[id="satWeeklyButton"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });