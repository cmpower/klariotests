var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.url)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .click('form[id="loginForm"]	[type="submit"]')
    .wait(5000)
    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //verify that device status is present
    .exists('[class="power_checkbox deviceControlCheckBox"]')
    .wait(2000)

    //click the device
    .click('[class="power_checkbox deviceControlCheckBox"]')
    .screenshot("DeviceStatusImage.png")
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
