/**
 * Created by ravishankar on 7/12/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    .wait(2000)
    //Login as user1
    .type('form[id="loginForm"] [name=userId]', 'ppcloud41@gmail.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ppcloud41$')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //search for the device
    .type('input[type="search"]', 'app4')
    .wait(2000)
    //Click on subowners of the appliance
    .click('i[class="fa fa-users fa-2x"]')
    .wait(2000)

    //Click on revoke
    .click('i[class="fa fa-times"]')
    .wait(2000)

    //click back at appliance list
    .click('a[href="#appliance"]')
    .wait(2000)
    //search for the device
    .type('input[type="search"]', 'app4')
    .wait(2000)
    //Click on subowners of the appliance
    .click('span[class="userPermissions c-teal"]')
    .wait(2000)
    //verify
    .exists('[class="removeUser btn btn-raised btn-sm btn-danger"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
