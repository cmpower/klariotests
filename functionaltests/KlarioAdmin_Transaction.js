/**
 * Created by ravishankar on 8/12/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //Click on appliance menu
    .click('[class="dropdown-toggle f-18"]')
    .wait(2000)

    //Choose Transaction from appliance menu
    .click('[href="#appliance-transaction-list"]')
    .wait(2000)

    //Search for a specific appliance
    .type('input[type="search"]', 'Appliance')
    .wait(2000)

    //click on show transaction button
    .click('[class="showApplianceTransaction btn btn-sm btn-raised btn-info"]')
    .wait(2000)

    //verify if the sorting class exists
    .exists('td[class="sorting_1"]')
    .wait(2000)

    //Click on a transaction
    .click('[class="getTransactionDetail btn btn-sm btn-raised btn-info"]')
    .wait(2000)
    //verify if few of the parameters exist
    .exists('h5[id="requestSentByUserId"]')
    .exists('h5[id="transactionId"]')
    .exists('h5[id="deviceCommandSent"]')
    .exists('h5[id="requestReceivedFromApp"]')
    .exists('h5[id="deviceStatusType"]')
    .exists('h5[id="deviceNewStatus"]')
    .wait(3000)

    //click on goback button
    .click('button[id="goBackTDBtn"]')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });


