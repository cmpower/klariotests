/**
 * Created by ravishankar on 28/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    .wait(4000)
    //Click on Settings on the Login page
    .click('a[href="#global-settings"]')
    .wait(2000)

    //click on radio button
    .click('[id="klarioNET"]')
    .wait(4000)

    //Go to login screen
    .click('a[href="#login"]')
    .wait(4000)

    //Click on Settings on the Login page
    .click('a[href="#global-settings"]')
    .wait(4000)

    //click on radio button
    .click('[id="klarioIN"]')
    .wait(4000)

    //Go to login screen
    .click('a[href="#login"]')
    .wait(4000)

    //Click on Settings on the Login page
    .click('a[href="#global-settings"]')
    .wait(4000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });