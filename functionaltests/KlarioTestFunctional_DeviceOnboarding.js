/**
 * Created by ravishankar on 17/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login as demo
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Verify if add device button exists
    .exists('showAddDeviceFormButton')
    .wait(2000)
    //Now click on the add device button
    .click('button[id="showAddDeviceFormButton"]')
    .wait(2000)

    //Type in serial ID and add device
    .type('input[placeholder="Serial No"]', '000002')
    .wait(2000)
    .click('button[type="submit"]')
    .wait(2000)

    //On the Appliance details screen provide appliance name and location
    .type('input[id="deviceLocation"]', 'tt2')
    .wait(2000)

    //click save
    .click('button[id="submitAddDeviceData"]')
    .wait(2000)

    //click on logoutButton after verifying if it exists
    .exists('logoutButton')
    .click('li[id="logoutButton"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
