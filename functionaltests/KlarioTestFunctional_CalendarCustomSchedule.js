/**
 * Created by ravishankar on 30/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Click calendar
    .click('i[class="fa fa-calendar fa-2x"]')
    .wait(2000)

    //click on DATE
    .click('span[class="calDate"]')
    .wait(4000)


    //Enter From time
    .click('input[id="fromCustomTime"]')
    .wait(2000)
    //Select from clock
    .click('div[id="timepicker-faceH"]')
    .wait(2000)
    .click('span[data="1"]')
    .wait(2000)
    .click('div[id="timepicker-faceM"]')
    .wait(2000)
    .click('span[data="1"]')
    .wait(2000)
    ////Click on Done
    //.click('span[id="timepicker-done-button"]')
    //.wait(2000)
    //
    ////Enter TO Time
    //.click('input[id="toCustomTime"]')
    //.wait(2000)

    ////Select from clock
    //.click('div[id="timepicker-faceH"]')
    //.wait(2000)
    //.click('span[data="1"]', '04:00 AM')
    //.wait(2000)
    //
    ////Click on Done
    //.click('span[id="timepicker-done-button"]')
    //.wait(2000)

    //SAVE
    .click('button[id="setCustomScheduleButton"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)

    });
