/**
 * Created by ravishankar on 15/12/16.
 */
var Nightmare = require('nightmare');
var nightmare = Nightmare({ show: true })
var urlin = require('./URLParameter.js');

nightmare
    .goto(urlin.urladmin)
    .wait(2000)
    //Login as demo
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="userId"]', 'admin')
    .type('form[class="form-horizontal fv-form fv-form-bootstrap"] [id="password"]', 'admin')
    .click('form[class="form-horizontal fv-form fv-form-bootstrap"]	[type="submit"]')
    .wait(2000)

    //click on add device after verifying if it exists
    .exists('[id="logout"]')

    //Click on appliance menu
    .click('[class="dropdown-toggle f-18"]')
    .wait(2000)

    //Choose wifi provisioning history from appliance menu
    .click('[href="#wifi-config-history"]')
    .wait(2000)

    //Search for a specific appliance
    .type('input[type="search"]', 'Appliance')
    .wait(2000)

    //click on show transaction button
    .click('[class="applianceWiFiInfoBtn btn btn-sm btn-raised btn-info"]')
    .wait(2000)

    //verify if the sorting class exists
    .exists('th[class="sorting_asc"]')
    .wait(2000)
    //click on Wifi Config button
    //.click('[class="applianceWiFiConfigInfoBtn btn btn-sm btn-raised btn-info"]')
    //.wait(2000)
    ////check if the parameter names exist
    //.exists('h5[id="ssid"]')
    //.exists('h5[id="encryptionType"]')
    //.exists('h5[id="selectedMode"]')
    //.exists('h5[id="ip"]')
    //.exists('h5[id="netmask"]')
    //.exists('h5[id="gateway"]')
    //.wait(2000)
    //
    ////close window
    //.click('[class="close"]')
    //.wait(2000)


    //click on goback button
    .click('button[id="goBackWDBtn"]')
    .wait(2000)

    //click on logoutButton
    .click('[id="logout"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });
