/**
 * Created by ravishankar on 16/11/16.
 */
var Nightmare = require('nightmare');
var nightmarenet = Nightmare({ show: true })
var urlnet1 = require('./URLParameter.js')

nightmarenet
    .goto(urlnet1.urlnet)
    //Login as demo
    .type('form[id="loginForm"] [name="userId"]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name="inputPassword"]', 'ph123')
    .wait(3000)
    .click('button[type="submit"]')
    .wait(5000)

    //Verify if add device button exists
    .exists('showAddDeviceFormButton')
    .wait(2000)
    //Now click on the add device button
    .click('button[id="showAddDeviceFormButton"]')
    .wait(2000)

    //click on logoutButton after verifying if it exists
    .exists('logoutButton')
    .click('li[id="logoutButton"]')
    .wait(3000)



    .evaluate(
    function () {
        return document.documentElement.innerText
    })
    .end()
    .then(function (result) {
        console.log(result)
    });