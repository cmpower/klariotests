/**
 * Created by ravishankar on 8/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Verify appliance link exists click it
    .exists('#appliance')
    .click('a[href="#appliance"]')
    .wait(2000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

     //Click settings button
    .click('i[class="fa fa-cog fa-2x"]')
    .wait(2000)

    //verify if CHANGE button exists
    .exists('button[class="btn text-center btn-raised btn-sm btn-primary"]')
    .wait(2000)

    //click Edit
    .click('button[class="btn text-center btn-raised btn-sm btn-primary"]')
    .wait(2000)

    //type appliance location
    .type('[name="applianceLocation"]', '***')
    .wait(2000)

    //click on submit/Edit
    .click('button[id="applianceInfoEditBtn"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });