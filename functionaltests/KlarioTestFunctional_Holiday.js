/**
 * Created by ravishankar on 8/11/16.
 */
var Nightmare = require('nightmare');
var urlin = require('./URLParameter.js');
var nightmare = Nightmare({ show: true })

nightmare
    .goto(urlin.url)
    //Login
    .type('form[id="loginForm"] [name=userId]', 'preethi@cloudmpower.com')
    .type('form[id="loginForm"] [name=inputPassword]', 'ph123')
    .wait(3000)
    .click('form[id="loginForm"] [type="submit"]')
    .wait(5000)

    //Search for device
    .type('input[type="search"]', 'new1')
    .wait(2000)

    //Click calendar
    .click('i[class="fa fa-calendar fa-2x"]')
    .wait(2000)

    //click on holidaytab
    .click('a[href="#HolidaysTab"]')
    .wait(2000)

    //verify if the holiday toggle button exists and click
    .exists('holidaySwitch')
    .wait(2000)
    .check('input[id="holidaySwitch"]')
    .wait(2000)

    //Go to Calendar tab and then come back to Holidays
    .click('a[href="#DateRangeTab"]')
    .wait(2000)
    .click('a[href="#HolidaysTab"]')
    .wait(2000)
    .exists('holidayListTBody')
    .wait(2000)

    //verify if holiday switch exists and toggle it back
    .exists('holidaySwitch')
    .wait(2000)
    .click('input[id="holidaySwitch"]')
    .wait(2000)

    .evaluate(
    function () {
        return document.documentElement.innerText

    })
    .end()
    .then(function (result) {
        console.log(result)
    });