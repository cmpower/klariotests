/**
 * Created by ravishankar on 16/11/16.
 */

(function(){
    "use strict";

    module.exports = function(){

        var Url = {
            "url":"https://test.klario.in/#login",
            "urlnet":'https://test.klario.net/#login',
            "urladmin":'https://www.klario.in/admin/#login',
            "urltest":'https://www.klario.in/test/index.html#login'
        };
        return Url ;
    }();
})();